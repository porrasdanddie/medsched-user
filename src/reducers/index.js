import doctorsReducer from './doctors-reducer'
import inquiriesReducer from './inquiries-reducer'
import patientsReducer from './patients-reducer'
import transactionsReducer from './transactions-reducer'
import slotsReducer from './slots-reducer'
import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'

export default combineReducers({
  doctors: doctorsReducer,
  inquiries: inquiriesReducer,
  patients: patientsReducer,
  transactions: transactionsReducer,
  slots: slotsReducer,
  form: formReducer
})
