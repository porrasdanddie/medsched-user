export default function reducer (state = {}, { type, payload }) {
  switch (type) {
    case 'FETCH_TRANSACTIONS_SUCCESS':
      return {
        ...state,
        transactions: payload
      }
    default:
      return state
  }
}
