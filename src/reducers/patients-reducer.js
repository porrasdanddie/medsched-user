export default function reducer (state = {}, { type, payload }) {
  switch (type) {
    case 'FETCH_PATIENTS_SUCCESS':
      return {
        ...state, 
        patients: payload
      }
    default:
      return state
  }
}
