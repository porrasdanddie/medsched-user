export default function reducer (state = {}, { type, payload }) {
  switch (type) {
    case 'FETCH_DOCTORS_SUCCESS':
      return {
        ...state,
        doctors: payload
      }
    default:
      return state
  }
}
