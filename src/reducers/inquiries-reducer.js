export default function reducer (state = {}, { type, payload }) {
  switch (type) {
    case 'FETCH_INQUIRIES_SUCCESS':
      return {
        ...state,
        inquiries: payload
      }
    default:
      return state
  }
}
