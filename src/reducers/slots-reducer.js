export default function reducer (state = {}, { type, payload }) {
  switch (type) {
    case 'CHECKED_SLOTS_LEFT':
      return {
        ...state,
        slots: payload
      }
    default:
      return state
  }
}
