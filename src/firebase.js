import * as firebase from "firebase"
import 'firebase/firestore'

firebase.initializeApp({
  apiKey: "AIzaSyBWuLNRGxQxAm9oIKgRe0YcKjAYwgaLxH4",
  authDomain: "medsched-b5090.firebaseapp.com",
  databaseURL: "https://medsched-b5090.firebaseio.com",
  projectId: "medsched-b5090",
  storageBucket: "medsched-b5090.appspot.com",
  messagingSenderId: "228490707048"
})

const database = firebase.firestore()
const authenticate = firebase.auth()

export { database, authenticate }
