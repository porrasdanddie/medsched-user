import { database as db } from '../firebase'

export const fetchedDoctors = (doctors) => {
  return {
    type: 'FETCH_DOCTORS_SUCCESS',
    payload: doctors
  }
}

export const fetchedInquiries = (inquiries) => {
  return {
    type: 'FETCH_INQUIRIES_SUCCESS',
    payload: inquiries
  }
}

export const fetchedTransactions = (transactions) => {
  return {
    type: 'FETCH_TRANSACTIONS_SUCCESS',
    payload: transactions
  }
}

export const fetchedPatients = (patients) => {
  return {
    type: 'FETCH_PATIENTS_SUCCESS',
    payload: patients
  }
}

export const fetchedNotifications = (notifications) => {
  return {
    type: 'FETCH_NOTIFICATIONS_SUCCESS',
    payload: notifications
  }
}

export const fetchedReservations = (reservations) => {
  return {
    type: 'FETCH_RESERVATIONS_SUCCESS',
    payload: reservations
  }
}

export function fetchDoctors () {
  return async (dispatch) => {
    let doctors = await db.collection('doctors').get()
    const doctorsList = []
    doctors.forEach((doctor) => {
      // console.log('RUNNING!!!')
      doctorsList.push(
        {data: doctor.data(), id: doctor.id}
      )
    })
    dispatch(fetchedDoctors(doctorsList))
  }
}

export function fetchInquiries (field, value) {
  console.log('HOY!', field, value)
  return async (dispatch) => {
    let inquiries = await db.collection('inquiries').where(field, '==', value).get()
    const inquiriesList = []
    inquiries.forEach((inquiry) => {
      inquiriesList.push(
        {data: inquiry.data(), id: inquiry.id}
      )
    })
    dispatch(fetchedInquiries(inquiriesList))
  }
}

export function fetchPatients (value) {
  console.log('VALUE ', value)
  return async (dispatch) => {
    let inquiries = await db.collection('inquiries').where('doctorID', '==', value).get()
    const patientsList = []
    inquiries.forEach((inquiry) => {
      const id = inquiry.data().patientID
      if (!patientsList.includes(id)) {
        patientsList.push(id)
      }
    })
    dispatch(fetchedPatients(patientsList))
  }
}

export function fetchTransactions (field, value) {
  console.log('VALUE ', value)
  return async (dispatch) => {
    let transactions = await db.collection('transactions').where(field, '==', value).get()
    const transactionsList = []
    transactions.forEach((transaction) => {
      transactionsList.push(
        {data: transaction.data(), id: transaction.id}
      )
    })
    dispatch(fetchedTransactions(transactionsList))
  }
}

export function fetchNotifications (value) {
  return async (dispatch) => {
    let notifications = await db.collection('notifications').where('patientID', '==', value).get()
    const notificationsList = []
    notifications.forEach((notification) => {
      notificationsList.push(
        {data: notification.data(), id: notification.id}
      )
    })
    dispatch(fetchedNotifications(notificationsList))
  }
}

export function fetchReservations (value) {
  return async (dispatch) => {
    let reservations = await db.collection('reservations').where('doctorID', '==', value).get()
    const reservationsList = []
    reservations.forEach((reservation) => {
      reservationsList.push(
        reservation.data()
      )
    })
    dispatch(fetchedNotifications(reservationsList))
  }
}
