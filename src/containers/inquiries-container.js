import { connect } from 'react-redux'
import Inquiries from '../components/Inquiries.jsx'
import { fetchInquiries } from '../actions/index'

const mapStateToProps = (state) => {
  console.log('data', state.inquiries.inquiries)
  return { inquiries: state.inquiries.inquiries }
}

const mapDispatchToProps = (dispatch) => ({
  fetchInquiries: (field, value) => dispatch(fetchInquiries(field, value)) 
})

export default connect(mapStateToProps, mapDispatchToProps)(Inquiries)
