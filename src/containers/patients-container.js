import { connect } from 'react-redux'
import PatientsView from '../components/PatientsView.jsx'
import { fetchPatients } from '../actions/index'

const mapStateToProps = (state) => {
  return { 
    patients: state.patients.patients
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchPatients: (value) => dispatch(fetchPatients(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(PatientsView)
