import { connect } from 'react-redux'
import Notifications from '../components/Notifications.jsx'
import { fetchNotifications } from '../actions/index'

const mapStateToProps = (state) => {
  return { notifications: state.notifications.notifications }
}

const mapDispatchToProps = (dispatch) => ({
  fetchNotifications: (value) => dispatch(fetchNotifications(value))
})

export default connect(mapStateToProps, mapDispatchToProps)(Notifications)
