import { connect } from 'react-redux'
import ReceiptsView from '../components/ReceiptsView.jsx'
import { fetchTransactions } from '../actions/index'

const mapStateToProps = (state) => {
  return { transactions: state.transactions.transactions }
}

const mapDispatchToProps = (dispatch) => ({
  fetchTransactions: (field, value) => dispatch(fetchTransactions(field, value))
})

export default connect(mapStateToProps, mapDispatchToProps)(ReceiptsView)
