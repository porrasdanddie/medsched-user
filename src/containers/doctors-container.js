import { connect } from 'react-redux'
import DoctorsView from '../components/DoctorsView.jsx'
import { fetchDoctors } from '../actions/index'
import { checkSlotsLeft } from '../components/extras/checkSlotsLeft.js'

const mapStateToProps = (state) => {
  return {
    doctors: state.doctors.doctors,
    slotsLeft: state.slots.slots
  }
}

const mapDispatchToProps = (dispatch) => ({
  fetchDoctors: () => dispatch(fetchDoctors()),
  checkSlotsLeft: (event, data) => dispatch(checkSlotsLeft(event, data))
})

export default connect(mapStateToProps, mapDispatchToProps)(DoctorsView)
