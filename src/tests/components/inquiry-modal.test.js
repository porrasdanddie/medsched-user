import React from 'react'
import { shallow } from '../enzymeSetup'
import InquiryModal from '../../components/InquiryModal.jsx'
import {
  Modal, Header
} from 'semantic-ui-react'

let wrapper
let dummyDoctor = {
  contactNumber: '09364771998',
  gender: 'male',
  location: 'Victorias City, Negros Occidental',
  name: 'Peter Sogono',
  schedule: 'MWF 12:00-4:00',
  specialization: 'Dermatologist'
}
let button = null

describe('<InquiryModal />', () => {
  beforeEach(() => {
    wrapper = shallow(<InquiryModal doctor={dummyDoctor} button={button} />)
  })

  it('renders itself', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('contains <Modal>', () => {
    expect(wrapper.find(Modal)).toHaveLength(1)
  })

  it('contains <Header>', () => {
    expect(wrapper.find(Header)).toHaveLength(1)
  })

  it('contains <Modal.Content>', () => {
    expect(wrapper.find(Modal.Content)).toHaveLength(1)
  })
})
