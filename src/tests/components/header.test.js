import React from 'react'
import { shallow } from '../enzymeSetup'
import HeaderView from '../../components/Header.jsx'
import { Menu, Button } from 'semantic-ui-react'

let wrapper

describe('<Header />', () => {
  beforeEach(() => {
    wrapper = shallow(<HeaderView />)
  })

  it('renders itself', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('contains <Menu>', () => {
    expect(wrapper.find(Menu)).toHaveLength(1)
  })

  it('contains 5 <Menu.Item>', () => {
    expect(wrapper.find(Menu.Item)).toHaveLength(5)
  })

  it('contains <Button>', () => {
    expect(wrapper.find(Button)).toHaveLength(1)
  })

  it('contains 2 <Button.Content>', () => {
    expect(wrapper.find(Button.Content)).toHaveLength(2)
  })
})
