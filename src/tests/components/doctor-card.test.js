import React from 'react'
import { shallow } from '../enzymeSetup'
import DoctorCard from '../../components/DoctorCard.jsx'
import { Card, Icon, Image } from 'semantic-ui-react'

let wrapper
let dummyDoctor = {
  contactNumber: '09364771998',
  gender: 'male',
  location: 'Victorias City, Negros Occidental',
  name: 'Peter Sogono',
  schedule: 'MWF 12:00-4:00',
  specialization: 'Dermatologist'
}
let dummySource = 'images/female-1.png'

describe('<DoctorCard />', () => {
  beforeEach(() => {
    wrapper = shallow(<DoctorCard
      source={dummySource}
      doctor={dummyDoctor} />)
  })

  it('renders itself', () => {
    expect(wrapper).toHaveLength(1)
  })

  it('contains <Card>', () => {
    expect(wrapper.find(Card)).toHaveLength(1)
  })

  it('contains <Image>', () => {
    expect(wrapper.find(Image)).toHaveLength(1)
  })

  it('contains 3 <Card.Content>', () => {
    expect(wrapper.find(Card.Content)).toHaveLength(3)
  })

  it('contains 5 <Icon>', () => {
    expect(wrapper.find(Icon)).toHaveLength(5)
  })

  it('contains 2 <Card.Meta>', () => {
    expect(wrapper.find(Card.Meta)).toHaveLength(2)
  })

  it('contains 1 <Card.Header>', () => {
    expect(wrapper.find(Card.Header)).toHaveLength(1)
  })

  it('contains 4 <Card.Description>', () => {
    expect(wrapper.find(Card.Description)).toHaveLength(4)
  })
})
