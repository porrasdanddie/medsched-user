import Enzyme, { shallow, render, mount } from 'enzyme'
import EnzymeAdapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new EnzymeAdapter() });

export { shallow, render, mount }
