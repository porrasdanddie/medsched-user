import reducers from './reducers/index'
import { createStore, applyMiddleware } from 'redux'
import promise from 'redux-promise'
import thunk from 'redux-thunk'
import {createLogger} from 'redux-logger'

export default createStore(reducers, applyMiddleware(thunk, createLogger()))