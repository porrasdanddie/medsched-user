import React from 'react'
import Filter from './Filter.jsx'
import { Icon, Grid } from 'semantic-ui-react'
import { genderFilter, specializationFilter, ctmFilter, provinceFilter } from './extras/filter-methods.js'
import { genderOptions, ctmOptions, provinceOptions, specializationOptions } from './extras/forms/form-options.js'

class Filters extends React.Component {

  constructor (props) {
    super(props)
    this.filterDoctors = this.filterDoctors.bind(this)
  }

  filterDoctors () {
    const dropdowns = document.querySelectorAll('.dropdown > div.text')
    const placeHolders = ['Gender', 'Specialization', 'City / Town / Municipality', 'Province']
    const filters = [genderFilter, specializationFilter, ctmFilter, provinceFilter]
    let doctors = this.props.doctors
    // console.log('prev doctors', doctors)
    const values = []

    dropdowns.forEach((dropdown) => {
      values.push(dropdown.innerText)
    })

    for (let i = 0; i < values.length; i++) {
      if (values[i] !== placeHolders[i] && values[i] !== 'Choose One') {
        doctors = doctors.filter((doctor) => {
          // console.log('result', filters[i](doctor.data, values[i]))
          return filters[i](doctor.data, values[i])
        })
      }
    }

    // console.log('filtered doctors', doctors)
    this.props.handleState(doctors)
  }

  render () {
    const options = [genderOptions, specializationOptions, ctmOptions, provinceOptions]
    const placeHolders = ['Gender', 'Specialization', 'City / Town / Municipality', 'Province']

    const filters = options.map((option, index) => {
      return <Filter
        options={option}
        placeholder={placeHolders[index]}
        key={index}
        filter={this.filterDoctors}
      />
    })

    return (
      <div style={{ marginBottom: '20px' }}>
        <center>
          <h4><Icon name='filter' />FILTER BY:</h4>
          <Grid columns='4' divided stretched>
            <Grid.Row>
              {filters}
            </Grid.Row>
          </Grid>
        </center>
      </div>
    )
  }
}

export default Filters
