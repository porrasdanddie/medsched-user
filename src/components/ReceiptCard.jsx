import React from 'react'
import solveProfit from './extras/profit-solver.js'
import { Card, Divider } from 'semantic-ui-react'
import { getDate, getTime } from './extras/date-formatter.js'
import { getDoctorDetails, getPatientDetails } from './extras/firestore/queries.js'

class ReceiptCard extends React.Component {
  constructor (props) {
    super(props)
    this.state = {doctor: {}, patient: {}}
  }

  componentDidMount () {
    const { transaction } = this.props

    getDoctorDetails(transaction.doctorID).then((value) => {
      this.setState({ doctor: value })
    })

    getPatientDetails(transaction.patientID).then((value) => {
      this.setState({ patient: value })
    })
  }

  render () {
    const { transaction, id } = this.props
    const { doctor, patient } = this.state

    return (
      <Card style={{ width: '310px' }} raised>
        <Card.Content>
          <strong><i>Receipt Number:</i></strong>
          <Card.Header>
            {id}
          </Card.Header>
          <Card.Meta>
            {getDate(transaction.timeStamp)}
          </Card.Meta>
          <Card.Meta>
            {getTime(transaction.timeStamp)}
          </Card.Meta>
        </Card.Content>
        <Card.Content textAlign='center'>
          <Divider horizontal><i>Remarks</i></Divider>
          <span>{transaction.remarks}</span>

          <Divider horizontal><i>{"Patient's Details"}</i></Divider>
          <strong>{patient.name}</strong><br/>
          <i>{patient.city}, {patient.province}</i>

          <Divider horizontal><i>{"Doctor's Detials"}</i></Divider>
          <strong>{doctor.name}</strong><br/>
          <i>{doctor.city}, {doctor.province}</i>

          <Divider horizontal><i>Details</i></Divider>
          <strong>Patient Fee: <i>{transaction.patientFee}</i></strong><br/>
          <strong>{"MedSched's Profit"}: <i>{solveProfit(transaction.patientFee)}</i></strong>
        </Card.Content>
      </Card>
    )
  }
}

export default ReceiptCard
