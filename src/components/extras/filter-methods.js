export const genderFilter = (doctor, value) => {
  return doctor.gender === value.toLowerCase()
}

export const specializationFilter = (doctor, value) => {
  return doctor.specialization === value
}

export const ctmFilter = (doctor, value) => {
  return doctor.city === value
}

export const provinceFilter = (doctor, value) => {
  return doctor.province === value
}
