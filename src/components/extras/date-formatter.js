import { format, distanceInWordsToNow } from 'date-fns'

export const getDate = (date) => {
  return format(date, 'MMMM D, YYYY')
}

export const getTime = (date) => {
  return format(date, 'h:mm:ss A')
}

export const getDayOfWeek = (date) => {
  return format(date, 'dddd')
}

export const getDistanceInWords = (date) => {
  return distanceInWordsToNow(date)
}
