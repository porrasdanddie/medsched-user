export const genderOptions = [
  { key: '0', value: '', text: 'Choose One' },
  { key: '1', value: 'male', text: 'Male' },
  { key: '2', value: 'female', text: 'Female' }
]

export const specializationOptions = [
  { key: '0', value: '', text: 'Choose One' },
  { key: '1', value: 'Ophthalmologist', text: 'Ophthalmologist' },
  { key: '2', value: 'Pediatrician', text: 'Pediatrician' },
  { key: '3', value: 'Internal Medicine', text: 'Internal Medicine' },
  { key: '4', value: 'Radiologist', text: 'Radiologist' },
  { key: '5', value: 'Cardiologist', text: 'Cardiologist' },
  { key: '6', value: 'Parasitologist', text: 'Parasitologist' }
]

export const ctmOptions = [
  { key: '0', value: '', text: 'Choose One' },
  { key: '1', value: 'Escalante', text: 'Escalante' },
  { key: '2', value: 'San Carlos', text: 'San Carlos' },
  { key: '3', value: 'Calatrava', text: 'Calatrava' },
  { key: '4', value: 'Toboso', text: 'Toboso' },
  { key: '5', value: 'Salvador Benedicto', text: 'Salvador Benedicto' },
  { key: '6', value: 'Cadiz', text: 'Cadiz' },
  { key: '7', value: 'Sagay', text: 'Sagay' },
  { key: '8', value: 'Manapla', text: 'Manapla' },
  { key: '9', value: 'Silay', text: 'Silay' },
  { key: '10', value: 'Talisay', text: 'Talisay' },
  { key: '11', value: 'Bacolod', text: 'Bacolod' },
  { key: '12', value: 'Victorias', text: 'Victorias' },
  { key: '13', value: 'Hinobaan', text: 'Hinobaan' }
]

export const provinceOptions = [
  { key: '0', value: '', text: 'Choose One' },
  { key: '1', value: 'Negros Occidental', text: 'Negros Occidental' },
  { key: '2', value: 'Negros Oriental', text: 'Negros Oriental' }
]
