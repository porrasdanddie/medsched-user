import React from 'react'
import { Field } from 'redux-form'
import { Form } from 'semantic-ui-react'
import { SelectField, InputField } from 'react-semantic-redux-form'

export const renderInputField = (icon, type, name, label) => (
  <Form.Field>
    <label>{label}</label>
    <Field
      icon={icon}
      iconPosition='left'
      type={type}
      name={name}
      component={InputField}
    />
  </Form.Field>
)

export const renderSelectField = (name, label, options) => (
  <Form.Field>
    <label>{label}</label>
    <Field
      component={SelectField}
      name={name}
      options={options}
      placeholder='Choose One'/>
  </Form.Field>
)
