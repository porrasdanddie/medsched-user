export const isValidName = (input) => (
  input && /(-?([A-Z].\s)?([A-Z][a-z]+)\s?)+([A-Z]'([A-Z][a-z]+))?/g.test(input)
)

export const isValidEmail = (input) => (
  input && /([\w\.\-_]+)?\w+@[\w-_]+(\.\w+){1,}/igm.test(input)
)

export const isValidPass = (input) => (
  input && /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/gm.test(input)
)

export const isValidPhoneNum = (input) => (
  input && /^(\+?63|0)?9\d{9}$/.test(input)
)

export const isValidFee = (input) => (
  input && /^[0-9]+(\.[0-9]{1,2})?$/gm.test(input)
)

export const isValidLicenseNum = (input) => (
  input && /^[0-9]{7}$/gm.test(input)
)
