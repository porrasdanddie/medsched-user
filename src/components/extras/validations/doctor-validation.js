import { isValidName, isValidEmail, isValidPass, isValidPhoneNum, 
  isValidFee, isValidLicenseNum } from './regex-tests.js'

export const validate = (values) => {
  console.log('VALUES', values)
  const errors = {}

  if (!isValidName(values.fullName)) {
    errors.fullName = 'Name is invalid!'
  }

  if (!isValidEmail(values.email)) {
    errors.email = 'Email format is invalid!'
  }

  if (!isValidPass(values.password)) {
    errors.password = 'Must be at least 8 characters with a number and an uppercase letter!'
  }

  if (!isValidPhoneNum(values.contactNumber)) {
    errors.contactNumber = 'Phone number is invalid!'
  }

  if (!isValidLicenseNum(values.licenseNumber)) {
    errors.licenseNumber = 'License is invalid!'
  }

  if (!isValidFee(values.fee)) {
    errors.licenseNumber = 'Fee is invalid!'
  }

  return errors
}
