import { database as db } from '../../firebase.js'
import { getDoctorDetails } from './firestore/queries.js'

export const checkedSlotsLeft = (slots) => {
  return {
    type: 'CHECKED_SLOTS_LEFT',
    payload: slots
  }
}

export const checkSlotsLeft = (event, data) => {
  return async (dispatch) => {
    const doctorID = localStorage.getItem('chosenDoctor')
    const date = data
    let slotsLeft
    const doctor = await getDoctorDetails(doctorID)
    const reservations = await db.collection('reservations').where('doctorID', '==', doctorID).get()
    let reservation
    reservations.forEach((res) => {
      if (res.data().date === date) {
        reservation = res.data()
      }
    })

    if (reservation) {
      slotsLeft = doctor.limit - reservation.inquiries.length
    } else {
      slotsLeft = doctor.limit
    }
    dispatch(checkedSlotsLeft(slotsLeft))
  }
}
