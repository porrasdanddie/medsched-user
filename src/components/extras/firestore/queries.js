import { database as db } from '../../../firebase.js'

export async function getDoctorDetails (id) {
  const details = await db.collection('doctors').doc(id).get()
  return details.data()
}

export async function getPatientDetails (id) {
  const details = await db.collection('patients').doc(id).get()
  return details.data()
}

export async function getInquiryDetails (id) {
  const details = await db.collection('inquiries').doc(id).get()
  return details.data()
}
