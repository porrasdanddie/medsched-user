import { database as db, authenticate } from '../../../firebase.js'
import swal from 'sweetalert'

async function addNewUser (email, password) {
  let user
  try {
    user = await authenticate.createUserWithEmailAndPassword(
      email, password
    )
  } catch (e) {
    user = e
  }
  return user
}

export async function addDoctor (values) {
  const user = await addNewUser(values.email, values.password)
  if (user.uid) {
    await db.collection('doctors').doc(user.uid).set({
      name: values.fullName,
      gender: values.gender,
      contactNumber: values.contactNumber,
      city: values.city,
      fee: values.fee,
      license: values.licenseNumber,
      province: values.province,
      specialization: values.specialization,
      yearsInService: values.yearsInService
    })
    swal(
      'Welcome to MedSched, ' + values.fullName + '!',
      'You may now login with your new account. Close sign up modal to continue', 
      'success'
    )
  } else {
    swal(
      'Sorry, Your Account cannot be created!',
      user.message,
      'error'
    )
  }
}

export async function addPatient (values) {
  const user = await addNewUser(values.email, values.password)
  if (user.uid) {
    await db.collection('patients').doc(user.uid).set({
      name: values.fullName,
      gender: values.gender,
      contactNumber: values.contactNumber,
      city: values.city,
      province: values.province,
      birthDate: values.birthdate
    })
    swal(
      'Welcome to MedSched, ' + values.fullName + '!',
      'You may now login with your new account. Close sign up modal to continue',
      'success'
    )
  } else {
    swal(
      'Sorry, Your Account cannot be created!',
      user.message,
      'error'
    )
  }
}
