import { database as db } from '../../../firebase.js'

export async function updateInquiry (id, status) {
  console.log('UPDATE', id, status)
  await db.collection('inquiries').doc(id).update({
    'status': status
  })
}
