import { database as db } from '../../../firebase.js'
import { updateInquiry } from './update-inquiry.js'
import swal from 'sweetalert'

export async function addTransaction (values) {
  console.log('VALUES', values)
  const transaction = await db.collection('transactions').add({
    timeStamp: new Date(),
    remarks: values.remarks,
    patientFee: values.patientFee,
    patientID: localStorage.getItem('chosenPatient'),
    doctorID: localStorage.getItem('userID')
  }).catch((error) => {
    swal(
      'Sorry, There was an error marking your appointment as done!',
      error,
      'error'
    )
  })
  if (transaction.id) {
    await updateInquiry(localStorage.getItem('chosenInquiry'), 'Done')
    swal(
      'Success!',
      'Appointment has been marked as done!',
      'success'
    )
  }
}
