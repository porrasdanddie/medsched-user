import { database as db } from '../../../firebase.js'
import { getDoctorDetails } from './queries.js'
import { getDate } from '../date-formatter.js'
import swal from 'sweetalert'

async function addToDB (values, doctorID) {
  const inquiry = await db.collection('inquiries').add({
    date: values.date,
    notes: values.notes,
    reason: values.reason,
    doctorID: doctorID,
    patientID: localStorage.getItem('user-id'),
    dateTransacted: new Date(),
    status: 'Pending'
  }).catch((error) => {
    swal(
      'Sorry, Inquiry was not sent!',
      error,
      'error'
    )
  })
  if (inquiry.id) {
    swal(
      'Congratulations!',
      'Inquiry has been sent!',
      'success'
    )
  }
}

export async function addInquiry (values) {
  console.log('values', values)
  const doctorID = localStorage.getItem('chosenDoctor')
  const doctor = await getDoctorDetails(doctorID)
  console.log('id', doctorID)
  console.log('doctor', doctor)
  const reservations = await db.collection('reservations').where('doctorID', '==', doctorID).get()
  let reservation
  reservations.forEach((res) => {
    if (res.data().date === values.date) {
      reservation = res.data()
    }
  })

  if (reservation) {
    if (reservation.inquiries.length < doctor.limit) {
      addToDB(values, doctorID)
    } else {
      const message = 'The doctor has already approved ' + doctor.limit +
        ' appointments scheduled on ' + getDate(values.date)
      swal(
        'Sorry, limit has been reached on the chosen date!',
        message,
        'error'
      )
    }
  } else {
    addToDB(values, doctorID)
  }
}
