import { database as db, authenticate } from '../../../firebase'
import { history } from '../history.js'
import swal from 'sweetalert'

const setUserDetails = (type, id, data) => {
  localStorage.setItem('user-type', type)
  localStorage.setItem('user-id', id)
  localStorage.setItem('user-data', JSON.stringify(data))
}

const removeUserDetails = () => {
  localStorage.removeItem('user')
  localStorage.removeItem('user-type')
  localStorage.removeItem('user-id')
  localStorage.removeItem('user-data')
}

export async function logIn (values) {
  const authenticated = await authenticate.signInWithEmailAndPassword(
    values.email,
    values.password
  ).catch((error) => {
    return error.message
  })

  if (authenticated.email) {
    localStorage.setItem(
      'user',
      JSON.stringify(authenticate.currentUser)
    )

    let user = await db.collection('doctors').doc(authenticated.uid).get()
    if (user.data()) {
      setUserDetails('Doctor', authenticated.uid, user.data())
      history.push('/viewInquiries')
    } else {
      user = await db.collection('patients').doc(authenticated.uid).get()
      setUserDetails('Patient', authenticated.uid, user.data())
      history.push('/browseDoctors')
    }
    swal('Logged In Successfuly', 'Hi, ' + user.data().name, 'success')
  } else {
    swal('Error Logging In', authenticated, 'error')
  }
}

export async function logOut () {
  try {
    await authenticate.signOut()
    removeUserDetails()
    swal('Logged Out Successfuly', 'Goodbye!', 'info')
    history.push('/')
  } catch (error) {
    swal('Error Logging Out', error.message, 'error')
  }
}