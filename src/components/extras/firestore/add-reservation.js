import { database as db } from '../../../firebase.js'
import { updateInquiry } from './update-inquiry.js'
import swal from 'sweetalert'

export async function addReservation (props) {
  console.log('RESERVE', props)
  const doctorID = localStorage.getItem('user-id')
  const doctor = JSON.parse(localStorage.getItem('user-data'))
  const reservations = await db.collection('reservations').where('doctorID', '==', doctorID).get()
  console.log('RESERVATIONS', reservations)
  let reservation
  let exists = false
  reservations.forEach((res) => {
    console.log('yay')
    if (res.data().date === props.inquiry.date) {
      reservation = res
      exists = true
    }
  })
  console.log('EXISTS', exists)

  if (exists) {
    console.log('GA EXIST KO!')
    const data = reservation.data()
    const id = reservation.id
    console.log('DATA ', data.inquiries.length, doctor.limit)
    if (data.inquiries.length < doctor.limit) {
      const arr = data.inquiries
      console.log('OUTDATED ARRAY', arr)
      arr.push(props.id)
      console.log('UPDATED ARRAY', arr)
      await db.collection('reservations').doc(id).set(
        { inquiries: arr },
        { merge: true }
      ).catch((error) => {
        swal(
          'Sorry, There was an error adding your reservation!',
          error,
          'error'
        )
      })
      await updateInquiry(props.id, 'Approved')
      swal(
        'Success!',
        'Reservation has been added!',
        'success'
      )
    } else {
      swal(
        'Sorry, Limit Reached for the specified Date!',
        'It is suggested that you decline the appointment',
        'error'
      )
    }
  } else {
    console.log('I AM INVOKED!')
    const reservation = await db.collection('reservations').add({
      doctorID: doctorID,
      date: props.inquiry.date,
      inquiries: [
        props.id
      ]
    }).catch((error) => {
      swal(
        'Sorry, There was an error adding your reservation!',
        error,
        'error'
      )
    })
    if (reservation.id) {
      await updateInquiry(props.id, 'Approved')
      swal(
        'Success!',
        'Reservation has been added!',
        'success'
      )
    }
  }
}
