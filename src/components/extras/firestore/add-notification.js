import { database as db } from '../../../firebase.js'
import swal from 'sweetalert'

export async function addNotif (values) {
  console.log('VALUES', values)
  const notif = await db.collection('notifications').add({
    subject: values.subject,
    message: values.message,
    doctorID: localStorage.getItem('user-id'),
    patientID: localStorage.getItem('chosenPatient')
  }).catch((error) => {
    swal(
      'Sorry, There was an error sending your notification!',
      error,
      'error'
    )
  })
  if (notif.id) {
    swal(
      'Success!',
      'Notification has been sent!',
      'success'
    )
  }
}
