import React from 'react'
import { Menu, Button, Icon } from 'semantic-ui-react'
import { Link } from 'react-router-dom'
import { logOut } from './extras/firestore/authenticate-user'
import { history } from './extras/history.js'

const renderMenuItems = (items, activeItem, handleItemClick) => (
  items.map((item, index) => {
    return <Menu.Item
      key={index}
      as={Link}
      to={item.path}
      name={item.name}
      active={activeItem === item.name}
      onClick={handleItemClick}
    />
  })
)

class NavBar extends React.Component {
  constructor (props) {
    super(props)
    this.state = { activeItem: history.location.pathname.substring(1) }
    this.handleItemClick = (e, { name }) => {
      this.setState({ activeItem: name })
    }
  }

  render () {
    const { activeItem } = this.state
    const patientItems = [
      { path: '/browseDoctors', name: 'browseDoctors' },
      { path: '/manageInquiries', name: 'manageInquiries' },
      { path: '/viewReceipts', name: 'viewReceipts' }
    ]
    const doctorItems = [
      { path: '/viewInquiries', name: 'viewInquiries' },
      { path: '/viewPatients', name: 'viewPatients' },
      { path: '/viewReceipts', name: 'viewReceipts' }
    ]
    let whichItems = localStorage.getItem('user-type') === 'Doctor' ? doctorItems : patientItems
    let user = localStorage.getItem('user-type')

    return (
      <Menu fixed='top' borderless pointing size='large'>
        <Menu.Item header>
          <Icon name='doctor' size='large' />
          <b><h3>MedSched</h3></b>
        </Menu.Item>

        {renderMenuItems(whichItems, activeItem, this.handleItemClick)}

        <Menu.Item position='right'>
          <Button animated='fade' negative onClick={logOut} >
            <Button.Content visible>
              <Icon name='user' />
              {user} Signed In
            </Button.Content>
            <Button.Content hidden>
              <Icon name='sign out' />
              Log Out
            </Button.Content>
          </Button>
        </Menu.Item>
      </Menu>
    )
  }
}

export default NavBar
