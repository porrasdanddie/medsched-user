import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Container } from 'semantic-ui-react'
import Patients from '../containers/patients-container.js'
import Receipts from '../containers/transaction-container.js'
import Inquiries from '../containers/inquiries-container.js'
// import Reservations from '../containers/reservations-container.js'

const DoctorSide = () => {
  return (
    <Container fluid>
      <Switch>
        {localStorage.getItem('user') && <Route path='/viewInquiries' component={Inquiries} />}
        {localStorage.getItem('user') && <Route path='/viewPatients' component={Patients} />}
        {localStorage.getItem('user') && <Route path='/viewReceipts' component={Receipts} />}
        {/* {localStorage.getItem('user') && <Route path='/viewReservations' component={Reservations} />} */}
      </Switch>
    </Container>
  )
}

export default DoctorSide
