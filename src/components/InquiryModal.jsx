import React from 'react'
import { Modal, Header } from 'semantic-ui-react'
import InquiryForm from './forms/InquiryForm.jsx'
import { addInquiry } from './extras/firestore/add-inquiry.js'

const InquiryModal = (props) => {
  return (
    <Modal trigger={props.button} basic size='small'
      closeOnEscape={false} closeOnRootNodeClick={false} closeIcon>
      <Header icon='doctor' size='huge' content={props.doctor.name + ' | ' +
        props.doctor.specialization} />
      <Modal.Content>
        <h4>Patient Limit Per Day: {props.doctor.limit} patients</h4>
        <h3>Fill up the form below to check availability of schedule:</h3>
        <InquiryForm id={props.id} onSubmit={addInquiry} slots={props.slots} 
          checkSlots={props.checkSlots}/>
      </Modal.Content>
    </Modal>
  )
}

export default InquiryModal
