import React from 'react'
import { Modal, Header, Divider } from 'semantic-ui-react'
import PaymentForm from './forms/PaymentForm.jsx'
import { addTransaction } from './extras/firestore/add-transaction.js'

const PaymentModal = (props) => {
  return (
    <Modal trigger={props.trigger} basic size='small'
      closeOnEscape={false} closeOnRootNodeClick={false} closeIcon>
      <Header icon='user' size='huge' content={props.patient.name} />
      <Modal.Content>
        <center><h3>{" Doctor's Base Fee "}: {props.doctor.fee}</h3></center>
        <Divider inverted />
        <h4>Fill up the form below to mark appointment as DONE:</h4>
        <PaymentForm onSubmit={addTransaction} doctor={props.doctor}
          patient={props.patient} patientID={props.patientID}
          inquiryID={props.inquiryID}/>
      </Modal.Content>
    </Modal>
  )
}

export default PaymentModal
