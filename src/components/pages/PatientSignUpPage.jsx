import React from 'react'
import { Modal, Header, Button, Icon } from 'semantic-ui-react'
import { Field, reduxForm } from 'redux-form'
import PatientSignUpForm from '../forms/PatientSignUpForm.jsx'
import { addPatient } from '../extras/firestore/add-user.js'

class SignUp extends React.Component {
  render() {
    const options = [
      { key: 'm', text: 'Male', value: 'male' },
      { key: 'f', text: 'Female', value: 'female' }
    ]

    return (
      <Modal trigger={this.props.button} size='large' basic
        closeOnEscape={false} closeOnRootNodeClick={false} closeIcon>
        <Header icon='add user' size='huge' content='Patient Sign Up Form' />
        <Modal.Content>
          <h3>Fill out the form below to create a new account:</h3>
          <PatientSignUpForm onSubmit={addPatient}/>
        </Modal.Content>
      </Modal>
    )
  }
}

export default SignUp