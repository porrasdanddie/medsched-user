import React from 'react'
import { Modal, Header } from 'semantic-ui-react'
import DoctorSignUpForm from '../forms/DoctorSignUpForm.jsx'
import { addDoctor } from '../extras/firestore/add-user.js'

const SignUp = (props) => {
  return (
    <Modal trigger={props.button} size='large' closeOnEscape={false}
      closeOnRootNodeClick={false} closeIcon>
      <Header icon='add user' size='huge' content='Doctor Sign Up Form' />
      <Modal.Content>
        <h3>Fill out the form below to create a new account:</h3>
        <DoctorSignUpForm onSubmit={addDoctor}/>
      </Modal.Content>
    </Modal>
    // </Transition>
  )
}

export default SignUp
