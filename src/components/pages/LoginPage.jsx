import React from 'react'
import LoginForm from '../../containers/login-container.js'
import { Grid, Message, Button, Icon, Container, Segment } from 'semantic-ui-react'
import { logIn } from '../extras/firestore/authenticate-user.js'
import PatientSignUp from './PatientSignUpPage.jsx'
import DoctorSignUp from './DoctorSignUpPage.jsx'

class Login extends React.Component {
  render() {
    return (
      <Container>
        <Grid textAlign='center' style={{ height: '100%' }} verticalAlign='middle'>
          <Grid.Column style={{ maxWidth: 550 }}>
            <Segment inverted>
              <LoginForm onSubmit={logIn} />
              <center>
                <h4>Don't have an account yet?</h4>
                <Button.Group>
                  <PatientSignUp button={<Button negative icon labelPosition='left'>
                    <Icon name='user' /> Sign Up as Patient
                  </Button>} />
                  <Button.Or />
                  <DoctorSignUp button={<Button positive icon labelPosition='right'>
                    <Icon name='doctor' /> Sign Up as Doctor
                  </Button>} />
                </Button.Group>
              </center>
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    )
  }
}

export default Login