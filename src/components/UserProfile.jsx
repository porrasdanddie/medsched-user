import React from 'react'
import { getDate } from './extras/date-formatter.js'
import { Header, Modal, Image, Divider } from 'semantic-ui-react'

const UserProfile = (props) => {
  const user = JSON.parse(localStorage.getItem('user-data'))
  const src = user.gender === 'male' ? '/images/male-1.jpg' : '/images/female-1.jpg'
  return (
    <Modal trigger={props.trigger} basic dimmer='blurring' size='tiny'>
      <Header icon='user' content='User Profile' />
      <Modal.Content>
        <center>
          <Image wrapped size='small' src={src} circular/>
          <Modal.Description style={{ textAlign: 'center' }}>
            <Divider inverted />
            <span style={{fontSize: '20px'}}>{user.name}</span><br/>
            <i>{localStorage.getItem('user-type').toUpperCase()}</i>
            <Divider inverted horizontal>Patient Details</Divider>
            <span>{user.city}, {user.province}</span>
            <Divider fitted hidden />
            <span>{user.contactNumber}</span>
            <Divider fitted inverted />
            <span>{getDate(user.birthDate)}</span>
          </Modal.Description>
        </center>
      </Modal.Content>
    </Modal>
  )
}

export default UserProfile
