import React from 'react'
import Receipt from './ReceiptCard.jsx'
import { Segment, Card } from 'semantic-ui-react'

class ReceiptsView extends React.Component {
  async componentDidMount () {
    const field = localStorage.getItem('user-type').toLowerCase() + 'ID'
    const value = localStorage.getItem('user-id')
    this.props.fetchTransactions(field, value)
  }

  render () {
    const { transactions } = this.props
    let transactionsList

    if (transactions) {
      transactionsList = transactions.map((transaction, index) => {
        return <Receipt transaction={transaction.data} id={transaction.id} key={index}/>
      })
    }

    return (
      <div style={{ marginLeft: '70px', marginRight: '70px', marginTop: '100px' }}>
        <Segment piled>
          <Card.Group centered>
            {transactionsList}
          </Card.Group>
        </Segment>
      </div>
    )
  }
}

export default ReceiptsView
