import React from 'react'
import FiltersView from './FiltersView.jsx'
import Doctor from './DoctorCard.jsx'
import UserProfile from './UserProfile.jsx'
import { Card, Segment, Label, Button } from 'semantic-ui-react'

class DoctorsView extends React.Component {

  constructor (props) {
    super(props)
    this.state = { doctors: [] }
    this.handleState = (doctors) => {
      this.setState({doctors})
    }
  }

  async componentDidMount () {
    await this.props.fetchDoctors()
    const doctors = this.props.doctors
    this.setState({doctors})
  }

  render () {
    const { doctors } = this.state
    const user = JSON.parse(localStorage.getItem('user-data'))
    const doctorsView = doctors.map((doctor, index) => {
      let source = 'images/' + doctor.data.gender + '-2.png'
      return <Doctor doctor={doctor.data} source={source} key={index} 
        id={doctor.id} checkSlots={this.props.checkSlotsLeft} 
        slotsLeft={this.props.slotsLeft}/>
    })

    return (
      <div style={{marginLeft: '70px', marginRight: '70px', marginTop: '100px'}}>
        <Segment piled>
          <UserProfile trigger={<Label as={Button} ribbon color='green' size='large' content={user.name} />} />
          <FiltersView
            handleState={this.handleState}
            doctors={this.props.doctors}
          />
          <Card.Group centered>
            {doctorsView}
          </Card.Group>
        </Segment>
      </div>
    )
  }
}

export default DoctorsView
