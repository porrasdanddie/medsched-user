import React from 'react'
import { reduxForm } from 'redux-form'
import { validate } from '../extras/validations/patient-validation.js'
import { Form, Divider, Label, Modal, Button, Icon } from 'semantic-ui-react'
import { renderInputField, renderSelectField } from '../extras/forms/form-fields.js'
import { genderOptions, ctmOptions, provinceOptions } from '../extras/forms/form-options.js'

const PatientSignUp = (props) => {
  const { handleSubmit, reset, submitting, pristine, submitSucceeded } = props

  if (submitSucceeded) {
    document.getElementById('reset').click()
  }

  return (
    <Form inverted onSubmit={handleSubmit} loading={submitting} >
      {/* Sign In Details */}
      <Label pointing='below' size='medium' color='green'>Enter Sign In Details Here</Label>
      <Form.Group widths='equal'>
        {renderInputField('mail', 'text', 'email', 'Email Address')}
        {renderInputField('lock', 'password', 'password', 'Password')}
      </Form.Group>

      <Divider inverted />

      {/* Other Details */}
      <Label pointing='below' size='medium' color='red'>Enter Other Details Here</Label>
      <Form.Group widths='equal'>
        {renderInputField('user circle outline', 'text', 'fullName', 'Full Name')}
        {renderSelectField('gender', 'Gender', genderOptions)}
      </Form.Group>
      <Form.Group widths='equal'>
        {renderInputField('phone square', 'text', 'contactNumber', 'Contact Number')}
        {renderInputField('calendar', 'date', 'birthdate', 'Birth Date')}
      </Form.Group>
      <Form.Group widths='equal'>
        {renderSelectField('city', 'City / Town / Municipality', ctmOptions)}
        {renderSelectField('province', 'Province', provinceOptions)}
      </Form.Group>

      <Divider inverted />

      <Modal.Actions style={{ float: 'right' }}>
        <Button color='red' type='button' id='reset' onClick={reset} disabled={pristine || submitting}>
          <Icon name='remove' /> Reset Values
        </Button>
        <Button color='green' inverted type='submit' disabled={pristine || submitting}>
          <Icon name='checkmark' /> Confirm Submission
        </Button>
      </Modal.Actions>
    </Form>
  )
}

export default reduxForm({ form: 'patientSignUpForm', validate })(PatientSignUp)
