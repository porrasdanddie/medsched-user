import React from 'react'
import { Form, Icon, Divider, Modal, Button } from 'semantic-ui-react'
import { reduxForm, Field } from 'redux-form'
import { InputField } from 'react-semantic-redux-form'

const setItems = (props) => {
  localStorage.setItem('chosenPatient', props.patientID)
  localStorage.setItem('chosenInquiry', props.inquiryID)
}

const PaymentForm = (props) => {
  const { handleSubmit, reset, submitting, submitSucceeded } = props
  if (submitSucceeded) {
    document.getElementById('reset').click()
  }
  const placeholder = 'Enter a value equal to or greater than ' + props.doctor.fee
  setItems(props)
  return (
    <Form onSubmit={handleSubmit} inverted>
      <Form.Group>
        <Form.Field disabled={submitting}>
          <Field
            type='text'
            name='remarks'
            style={{ width: '680px' }}
            label='Remarks'
            placeholder='Enter remarks'
            component={InputField}
          />

          <Divider hidden />

          <Field
            type='number'
            name='patientFee'
            min={props.doctor.fee}
            style={{ width: '680px' }}
            label='Patient Fee'
            placeholder={placeholder}
            component={InputField}
          />
        </Form.Field>
      </Form.Group>
      <Divider inverted />
      <Modal.Actions style={{ float: 'right' }}>
        <Button color='red' type='button' id='reset' onClick={reset}>
          <Icon name='remove' /> Reset Values
        </Button>
        <Button color='green' inverted>
          <Icon name='checkmark' /> Mark as Done
        </Button>
      </Modal.Actions>
    </Form>
  )
}

export default reduxForm({ form: 'paymentForm' })(PaymentForm)
