import React from 'react'
import { Form, Icon, Divider, Label, Modal, Button } from 'semantic-ui-react'
import { reduxForm, Field } from 'redux-form'
import { InputField, TextAreaField } from 'react-semantic-redux-form'

class InquiryForm extends React.Component {
  constructor (props) {
    super(props)
    this.handleChange = (event, data) => {
      console.log('INVOKED', data)
      this.props.checkSlots(event, data)
    }
  }

  render () {
    const { handleSubmit, reset, submitting } = this.props
    localStorage.setItem('chosenDoctor', this.props.id)

    let slots
    
    if (this.props.slots) {
      slots = this.props.slots
    }
    console.log('SLOTS', this.props.slots)
    return (
      <Form onSubmit={handleSubmit}>
        <Form.Group>
          <Form.Field disabled={submitting}>
            <Label size='large' color='red'>
              Date of Appointment
            </Label><br/>
            <Field
              type='date'
              name='date'
              style={{ width: '680px' }}
              onChange={this.handleChange}
              component={InputField}
            />
            <span>Slots Left: {slots}</span>
            <Divider hidden />

            <Label size='large' color='blue'>
              Reason for Visit
            </Label><br/>
            <Field
              type='text'
              name='reason'
              style={{ width: '680px' }}
              component={InputField}
            />

            <Divider hidden />

            <Label size='large' color='green'>
              Notes for the Doctor
            </Label><br/>
            <Field
              type='text'
              name='notes'
              style={{ width: '680px' }}
              component={TextAreaField}
            />
          </Form.Field>
        </Form.Group>
        <Divider inverted />
        <Modal.Actions style={{ float: 'right' }}>
          <Button color='red' type='button' id='reset' onClick={reset}>
            <Icon name='remove' /> Reset Values
          </Button>
          <Button color='green' inverted>
            <Icon name='checkmark' /> Submit Inquiry
          </Button>
        </Modal.Actions>
      </Form>
    )
  }
}

export default reduxForm({ form: 'inquiryForm' })(InquiryForm)
