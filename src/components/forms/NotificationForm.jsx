import React from 'react'
import { Form, Icon, Divider, Modal, Button } from 'semantic-ui-react'
import { reduxForm, Field } from 'redux-form'
import { InputField, TextAreaField } from 'react-semantic-redux-form'

const EmailForm = (props) => {
  const { handleSubmit, reset, submitting, submitSucceeded } = props

  if (submitSucceeded) {
    document.getElementById('reset').click()
  }

  return (
    <Form onSubmit={handleSubmit} inverted>
      <Form.Group>
        <Form.Field disabled={submitting}>
          <Field
            type='text'
            name='subject'
            style={{ width: '680px' }}
            label='Subject'
            placeholder='Enter subject'
            component={InputField}
          />

          <Divider hidden />

          <Field
            name='message'
            style={{ width: '680px' }}
            label='Message'
            placeholder='Enter your message here'
            component={TextAreaField}
          />
        </Form.Field>
      </Form.Group>
      <Divider inverted />
      <Modal.Actions style={{ float: 'right' }}>
        <Button color='red' type='button' id='reset' onClick={reset}>
          <Icon name='remove' /> Reset Values
        </Button>
        <Button color='green' inverted>
          <Icon name='checkmark' /> Send Notification
        </Button>
      </Modal.Actions>
    </Form>
  )
}

export default reduxForm({ form: 'emailForm' })(EmailForm)
