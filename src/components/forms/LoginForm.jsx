import React from 'react'
import { Form, Button, Divider, Icon, Header } from 'semantic-ui-react'
import { Field } from 'redux-form'
import { InputField } from 'react-semantic-redux-form'

const renderInputField = (icon, name, placeholder, type, submitting) => (
  <Form.Field style={{ width: '350px' }}>
    <Field
      icon={icon}
      iconPosition='left'
      type={type}
      name={name}
      placeholder={placeholder}
      component={InputField}
      disabled={submitting}
    />
  </Form.Field>
)

const LoginForm = (props) => {
  const { handleSubmit, submitting, pristine } = props

  return (
    <div>
      <Header as='h2' icon style={{ color: 'white' }}>
        <Icon name='doctor' />
          MedSched
        <Header.Subheader style={{ color: 'white' }}>
          An application designed to connect doctors to patients to lessen the hassle
           of setting appointments and lining up in long queues
        </Header.Subheader>
      </Header>
      <Divider inverted />
      <center>
        <h4>Please sign in to continue!</h4>
      </center><br />
      <Form as='form' onSubmit={handleSubmit}>
        <center>
          {renderInputField('mail', 'email', 'Email Address', 'text', submitting)}
          {renderInputField('lock', 'password', 'Password', 'password', submitting)}
        </center><br />
        <Button
          size='large'
          type='submit'
          disabled={pristine || submitting}
          inverted >
          <Icon name='sign in' />
          Sign In
        </Button>
        <Divider inverted />
      </Form>
    </div>
  )
}

export default LoginForm
