import React from 'react'
import { reduxForm } from 'redux-form'
import { validate } from '../extras/validations/doctor-validation.js'
import { renderInputField, renderSelectField } from '../extras/forms/form-fields.js'
import { Form, Label, Modal, Button, Icon, Divider } from 'semantic-ui-react'
import { genderOptions, ctmOptions, provinceOptions, specializationOptions } from '../extras/forms/form-options.js'

const DoctorSignUp = (props) => {
  const { handleSubmit, reset, submitting, pristine, submitSucceeded } = props

  if (submitSucceeded) {
    document.getElementById('reset').click()
  }

  return (
    <Form inverted onSubmit={handleSubmit} loading={submitting}>
      {/* Sign In Details */}
      <Label pointing='below' size='medium' color='green'>Enter Sign In Details Here</Label>
      <Form.Group widths='equal'>
        {renderInputField('mail', 'text', 'email', 'Email Address')}
        {renderInputField('lock', 'password', 'password', 'Password')}
      </Form.Group>

      <Divider inverted />

      {/* Other Details */}
      <Label pointing='below' size='medium' color='red'>Enter Other Details Here</Label>
      <Form.Group widths='equal'>
        {renderInputField('user circle outline', 'text', 'fullName', 'Full Name')}
        {renderSelectField('gender', 'Gender', genderOptions)}
      </Form.Group>
      <Form.Group widths='equal'>
        {renderInputField('phone square', 'text', 'contactNumber', 'Contact Number')}
        {renderInputField('money', 'text', 'fee', 'Check Up Fee')}
        {renderSelectField('specialization', 'Specialization', specializationOptions)}
      </Form.Group>
      <Form.Group widths='equal'>
        {renderInputField('calendar', 'text', 'yearsInService', 'Years In Service')}
        {renderInputField('drivers license', 'text', 'licenseNumber', 'License Number')}
      </Form.Group>

      <Divider inverted />

      {/* Clinic Details */}
      <Label pointing='below' size='medium' color='blue'>Enter Clinic Location Details</Label>
      <Form.Group widths='equal'>
        {renderSelectField('city', 'City / Town / Municipality', ctmOptions)}
        {renderSelectField('province', 'Province', provinceOptions)}
      </Form.Group>

      <Divider inverted />

      <Modal.Actions style={{ float: 'right' }}>
        <Button color='red' type='button' id='reset' onClick={reset} disabled={pristine || submitting}>
          <Icon name='remove' /> Reset Values
        </Button>
        <Button color='green' inverted type='submit' disabled={pristine || submitting}>
          <Icon name='checkmark' /> Confirm Submission
        </Button>
      </Modal.Actions>
    </Form>
  )
}

export default reduxForm({ form: 'doctorSignUpForm', validate })(DoctorSignUp)
