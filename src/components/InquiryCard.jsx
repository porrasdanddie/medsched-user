import React from 'react'
import { updateInquiry } from './extras/firestore/update-inquiry.js'
import { addReservation } from './extras/firestore/add-reservation.js'
import { isAfter } from 'date-fns'
import PaymentModal from './PaymentModal.jsx'
import { getDate, getDayOfWeek, getTime, getDistanceInWords } from './extras/date-formatter.js'
import { getDoctorDetails, getPatientDetails } from './extras/firestore/queries.js'
import { Card, Image, Divider, Label, Button, Icon } from 'semantic-ui-react'

const renderCardForPatient = (doctor, patient, inquiry, props) => (
  <Card style={{width: '310px'}} raised>
    <Card.Content>
      <Label attached='bottom right' size='large'
        color={getColor(inquiry.status)}>
        {inquiry.status}
      </Label>
      <Image floated='right' size='mini' src={getImageSource(doctor.gender)} circular />
      <Card.Header>
        {doctor.name}
      </Card.Header>
      <Card.Meta>
        {doctor.specialization}
      </Card.Meta>
      <Divider />
      <Card.Description textAlign='center'>
        <Divider horizontal>Appointment Date</Divider>
        <time>{getDate(inquiry.date)}</time>, <i>{getDayOfWeek(inquiry.date)}</i>
      </Card.Description>

      <Divider hidden/>
      <Card.Description textAlign='center'>
        <Divider horizontal>Reason for Visit</Divider>
        {inquiry.reason}
      </Card.Description>

      <Divider hidden/>
      <Card.Description textAlign='center'>
        <Divider horizontal>Notes for the Doctor</Divider>
        {inquiry.notes}
      </Card.Description>
    </Card.Content>
    <Card.Content extra>
      <Card.Meta>{getDate(inquiry.dateTransacted)}</Card.Meta>
      <Card.Meta>{getTime(inquiry.dateTransacted)}</Card.Meta>
      <Card.Meta>{patient.name}</Card.Meta>
    </Card.Content>
  </Card>
)

const renderCardForDoctor = (doctor, patient, inquiry, props) => (
  <Card style={{width: '310px'}} raised>
    <Label attached='top right' pointing='below'>added {getDistanceInWords(inquiry.dateTransacted)} ago</Label>
    <Card.Content>
      {isEligible(inquiry.status, inquiry.date) &&
        <PaymentModal trigger={<Label corner='left' as='a' color='green'>
          <Icon name='check' />
        </Label>} doctor={doctor} patient={patient} patientID={inquiry.patientID} inquiryID={props.id}/>}
      <Image floated='right' size='mini' src='/images/female-2.png' circular />
      <Card.Header>
        {patient.name}
      </Card.Header>
      <Card.Meta>{getDate(inquiry.dateTransacted)}</Card.Meta>
      <Card.Meta>{getTime(inquiry.dateTransacted)}</Card.Meta>
      <Divider />

      <Card.Description textAlign='center'>
        <Divider horizontal><i>Appointment Date</i></Divider>
        <time>{getDate(inquiry.date)}</time>, <i>{getDayOfWeek(inquiry.date)}</i>
      </Card.Description>

      <Divider fitted hidden/>
      <Card.Description textAlign='center'>
        <Divider horizontal><i>Reason for Visit</i></Divider>
        {inquiry.reason}
      </Card.Description>

      <Divider fitted hidden/>
      <Card.Description textAlign='center'>
        <Divider horizontal><i>Notes for the Doctor</i></Divider>
        {inquiry.notes}
      </Card.Description>
    </Card.Content>

    <Card.Content extra textAlign='center'>
      <b>Current Status: </b>
      <Label content={inquiry.status} color={getColor(inquiry.status)} size='large' basic/>
      <Divider />
      <Button.Group>
        <Button positive
          disabled={isDisabled(inquiry.status)}
          onClick={updateStatus.bind(null, props, 'Approved')}>
          Approve
        </Button>
        <Button.Or />
        <Button negative
          disabled={isDisabled(inquiry.status)}
          onClick={updateStatus.bind(null, props, 'Declined')}>
          Decline
        </Button>
      </Button.Group>
    </Card.Content>
  </Card>
)

const updateStatus = async (props, status) => {
  if (status === 'Approved') {
    await addReservation(props)
  } else {
    await updateInquiry(props.id, status)
  }
  const field = localStorage.getItem('user-type').toLowerCase() + 'ID'
  await props.fetch(field, localStorage.getItem('user-id'))
  props.handleState()
}

const isDisabled = (status) => {
  return ['Approved', 'Done', 'Declined'].includes(status)
}

const isEligible = (status, datetime) => {
  const isApproved = status === 'Approved'
  const after = isAfter(new Date(), datetime)
  return isApproved && after
}

const getColor = (status) => {
  let color = ''
  switch (status) {
    case 'Pending' :
      color = 'grey'
      break
    case 'Approved':
      color = 'green'
      break
    case 'Declined':
      color = 'red'
      break
    case 'Done':
      color = 'blue'
      break
  }

  return color
}

const getImageSource = (gender) => {
  const src = gender === 'male'
    ? '/images/male-2.png' : '/images/female-2.png'
  return src
}

class Inquiry extends React.Component {
  constructor (props) {
    super(props)
    this.state = {doctor: {}, patient: {}}
  }

  componentDidMount () {
    const { inquiry } = this.props

    getDoctorDetails(inquiry.doctorID).then((value) => {
      this.setState({ doctor: value })
    })

    getPatientDetails(inquiry.patientID).then((value) => {
      this.setState({ patient: value })
    })
  }

  render () {
    const { inquiry } = this.props
    const { doctor, patient } = this.state
    const whichCard = localStorage.getItem('user-type') === 'Doctor'
      ? renderCardForDoctor : renderCardForPatient
    return (
      whichCard(doctor, patient, inquiry, this.props)
    )
  }
}

export default Inquiry
