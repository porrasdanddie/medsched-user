import React from 'react'
import PatientSide from './PatientSide.jsx'
import DoctorSide from './DoctorSide.jsx'
import Header from './Header.jsx'
import Login from './pages/LoginPage.jsx'
import { Route } from 'react-router-dom'

const Index = () => {
  return (
    <div>
      {!localStorage.getItem('user') && <Route exact path='/' component={Login} />}
      {localStorage.getItem('user') && <Header />}
      {localStorage.getItem('user-type') === 'Patient' && <PatientSide />}
      {localStorage.getItem('user-type') === 'Doctor' && <DoctorSide />}
    </div>
  )
}

export default Index
