import React from 'react'
import InquiryModal from './InquiryModal.jsx'
import store from '../store'
import { checkedSlotsLeft } from './extras/checkSlotsLeft.js'
import { Card, Icon, Image, Button, Divider } from 'semantic-ui-react'

const clearPreviousState = () => {
  console.log('I AM INVOKED!')
  store.dispatch(checkedSlotsLeft(null))
}

const Doctor = (props) => {
  const { source, doctor, id } = props
  return (
    <Card style={{ width: '200px' }}>
      <Image src={source} />
      <Card.Content>
        <Card.Header>{doctor.name}</Card.Header>
        <Card.Meta><b>{doctor.schedule}</b></Card.Meta>
        <Card.Meta>Clinic is stationed in <i>{doctor.city}, {doctor.province}</i></Card.Meta>
        <Divider />
        <Card.Description><Icon name='doctor' /> <b>{doctor.specialization}</b></Card.Description>
        <Card.Description><Icon name='drivers license' /> {doctor.license}</Card.Description>
        <Card.Description><Icon name='money' /> {doctor.fee} pesos</Card.Description>
        <Card.Description><Icon name='calendar' /> {doctor.yearsInService} years in service</Card.Description>
      </Card.Content>

      <Card.Content extra>
        <a>
          <Icon name='phone square' />
          {doctor.contactNumber}
        </a>
      </Card.Content>

      <Card.Content extra>
        <InquiryModal
          button={<Button floated='right' size='tiny'
            onClick={clearPreviousState}
            color='red' >Inquire</Button>}
          doctor={doctor}
          id={id}
          checkSlots={props.checkSlots}
          slots={props.slotsLeft}
        />
      </Card.Content>
    </Card>
  )
}

export default Doctor
