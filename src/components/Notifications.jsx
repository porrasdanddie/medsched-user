import React from 'react'
import { getDoctorDetails } from './extras/firestore/queries.js'
import { Card, Segment, Divider } from 'semantic-ui-react'

class Notifications extends React.Component {
  async componentDidMount () {
    const patientID = localStorage.getItem('userID')
    await this.props.fetchNotifications(patientID)
  }

  render () {
    const { notifications } = this.props
    let notificationList

    if (notifications) {
      notificationList = notifications.map((notification, index) => {
        let doctor
        getDoctorDetails(this.props.notification.doctorID).then((value) => {
          doctor = value
        })
        return (
          <Card style={{ width: '190px' }} key={index}>
            <Card.Content>
              <Card.Header style={{ fontSize: '15px' }}>{doctor.name}</Card.Header>
            </Card.Content>
            <Card.Content>
              <Divider horizontal>Subject</Divider>
              <Card.Description>{notification.subject}</Card.Description>
              <Divider horizontal>Message</Divider>
              <Card.Description>{notification.message}</Card.Description>
            </Card.Content>
          </Card>
        )
      })
    }

    return (
      <div style={{ marginLeft: '70px', marginRight: '70px', marginTop: '100px' }}>
        <Segment piled padded>
          <Card.Group centered>
            {notificationList}
          </Card.Group>
        </Segment>
      </div>
    )
  }
}

export default Notifications
