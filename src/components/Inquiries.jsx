import React from 'react'
import Inquiry from './InquiryCard.jsx'
import { Segment, Card } from 'semantic-ui-react'

class Inquiries extends React.Component {
  constructor (props) {
    super(props)
    this.state = { inquiries: [] }
    this.handleState = () => {
      const inquiries = this.props.inquiries
      this.setState({ inquiries })
    }
  }

  async componentDidMount () {
    const field = localStorage.getItem('user-type').toLowerCase() + 'ID'
    await this.props.fetchInquiries(field, localStorage.getItem('user-id'))
    const inquiries = this.props.inquiries
    this.setState({ inquiries })
  }

  render () {
    const { inquiries } = this.state
    console.log('inq', inquiries)
    const inquiriesList = inquiries.map((inquiry, index) => {
      return <Inquiry inquiry={inquiry.data} key={index} id={inquiry.id}
        handleState={this.handleState} fetch={this.props.fetchInquiries}
        inquiries={this.props.inquiries} />
    })

    return (
      <div style={{ marginLeft: '80px', marginRight: '80px', marginTop: '100px' }}>
        <Segment piled>
          <Card.Group centered>
            {inquiriesList}
          </Card.Group>
        </Segment>
      </div>
    )
  }
}

export default Inquiries
