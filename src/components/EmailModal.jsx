import React from 'react'
import { Modal, Header } from 'semantic-ui-react'
import NotificationForm from './forms/NotificationForm.jsx'
import { addNotif } from './extras/firestore/add-notification.js'

const EmailModal = (props) => {
  localStorage.setItem('chosenPatient', props.patient)
  return (
    <Modal trigger={props.button} basic size='small'
      closeOnEscape={false} closeOnRootNodeClick={false} closeIcon>
      <Header icon='doctor' size='huge' content={props.patient.name} />
      <Modal.Content>
        <h3>Fill up the form below to notify the patient:</h3>
        <NotificationForm onSubmit={addNotif}/>
      </Modal.Content>
    </Modal>
  )
}

export default EmailModal
