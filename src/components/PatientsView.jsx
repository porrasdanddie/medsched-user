import React from 'react'
import { getPatientDetails } from './extras/firestore/queries.js'
import { getDate } from './extras/date-formatter.js'
import EmailForm from './EmailModal'
import { Card, Segment, Icon, Image, Button } from 'semantic-ui-react'

class PatientsView extends React.Component {

  constructor (props) {
    super(props)
    this.state = { patients: [] }
  }

  async componentDidMount () {
    const doctorID = localStorage.getItem('userID')
    await this.props.fetchPatients(doctorID)
    const patientIDs = this.props.patients
    let patients = []
    for (const id of patientIDs) {
      await getPatientDetails(id).then((value) => {
        patients.push({value: value, id: id})
      })
    }
    this.setState({ patients })
  }

  render () {
    const { patients } = this.state

    const patientsList = patients.map((patient, index) => {
      
      const src = patient.value.gender === 'male' ? '/images/male-2.png' : '/images/female-2.png'
      return (
        <Card style={{ width: '190px' }} key={index}>
          <Image src={src} size='small' centered />
          <Card.Content>
            <Card.Header style={{ fontSize: '15px' }}>{patient.value.name}</Card.Header>
            <Card.Meta><i>{patient.value.city}, {patient.value.province}</i></Card.Meta>
          </Card.Content>
          <Card.Content extra>
            <Card.Description><Icon name='phone square' /> <b>{patient.value.contactNumber}</b></Card.Description>
            <Card.Description><Icon name='calendar' /> {getDate(patient.value.birthDate)}</Card.Description>
          </Card.Content>
          {/* <Card.Content extra>
            <EmailForm button={<Button positive floated='right'>Contact Patient</Button>} patient={patient.id} />
          </Card.Content> */}
        </Card>
      )
    })

    return (
      <div style={{ marginLeft: '70px', marginRight: '70px', marginTop: '100px' }}>
        <Segment piled padded>
          <Card.Group centered>
            {patientsList}
          </Card.Group>
        </Segment>
      </div>
    )
  }
}

export default PatientsView
