import React from 'react'
import { Dropdown, Grid } from 'semantic-ui-react'

const Filter = (props) => {
  return (
    <Grid.Column>
      <Dropdown
        className='dropdown'
        options={props.options}
        search button fluid
        onClose={props.filter}
        style={{ width: '250px' }}
        placeholder={props.placeholder}
        scrolling
      />
    </Grid.Column>
  )
}

export default Filter
