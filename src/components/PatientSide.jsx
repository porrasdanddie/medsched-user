import React from 'react'
import { Switch, Route } from 'react-router-dom'
import { Container } from 'semantic-ui-react'
import DoctorsView from '../containers/doctors-container.js'
import ReceiptsView from '../containers/transaction-container.js'
import Inquiries from '../containers/inquiries-container.js'
import Notifications from '../containers/notifications-container.js'

export default class Body extends React.Component {
  render () {
    return (
      <Container fluid>
        <Switch>
          {localStorage.getItem('user') && <Route path='/browseDoctors' component={DoctorsView} />}
          {localStorage.getItem('user') && <Route path='/manageInquiries' component={Inquiries} />}
          {localStorage.getItem('user') && <Route path='/viewReceipts' component={ReceiptsView} />}
          {localStorage.getItem('user') && <Route path='/notifications' component={Notifications} />}
        </Switch>
      </Container>
    )
  }
}
