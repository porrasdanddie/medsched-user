import React from 'react'
import ReactDOM from 'react-dom'
import Index from './src/components/Index.jsx'
import store from './src/store'
import { Provider } from 'react-redux'
import { Router } from 'react-router-dom'
import { history } from './src/components/extras/history'

const container = document.getElementById('container')

ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Index />
    </Router>
  </Provider>,
  container
)
