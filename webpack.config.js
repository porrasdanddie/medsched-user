const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: ['./frontend.js'],
  output: {
    filename: 'bundle.js',
    publicPath: '/',
    path: path.join(process.cwd(), 'public')
  },
  module: {
    rules: [{
      test: /\.jsx?/,
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
      }
    }]
  },
  resolve: {
    extensions: ['.js', '.jsx', '.json']
  },
  devServer: {
    historyApiFallback: true,
    publicPath: '/',
    contentBase: './public',
    compress: true
  },
  mode: 'development'
}