// const express = require('express')
// const bodyParser = require('body-parser')
// const path = require('path')
// const webpack = require('webpack')
// const config = require('./webpack.config')
// const devMiddleware = require('webpack-dev-middleware')
// const hotMiddleware = require('webpack-hot-middleware')
// const MongoClient = require('mongodb')
// const url = 'mongodb://localhost:27017'
// const dbName = 'enterp-project'
// const port = 8000
// let db

// async function startServer() {
//   const app = express()

//   if (process.env.NODE_ENV !== 'production') {
//     const compiler = webpack(config)
    
//     app
//       .use(devMiddleware(compiler, {
//         noInfo: true, publicPath: config.output.publicPath
//       }))
//       .use(hotMiddleware(compiler))
//   }

//   const client = await MongoClient.connect(url)
//   db = client.db(dbName)

//   app
//     .use(bodyParser.urlencoded({ extended: true }))
//     .use(bodyParser.json())
//     .use('/', express.static(path.join(process.cwd(), 'public')))
//     .get('/patients', getPatients)
//     .get('/doctors', getDoctors)
//     .get('/transactions', getTransactions)
//     .listen(port, () => { console.log('server live') })
// }

// async function getPatients (request, response) {
//   try {
//     const patients = await db.collection('patients').find().toArray()
//     response.json(patients)
//   } catch (e) {
//     throw e
//   }
// }

// async function getDoctors (request, response) {
//   try {
//     const doctors = await db.collection('doctors').find().toArray()
//     response.json(doctors)
//   } catch (e) {
//     throw e
//   }
// }

// async function getTransactions (request, response) {
//   try {
//     const transactions = await db.collection('transactions').find().toArray()
//     response.json(transactions)
//   } catch (e) {
//     throw e
//   }
// }

// startServer()